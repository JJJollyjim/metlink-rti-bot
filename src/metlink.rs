use crate::geom::Point;
use failure;
use reqwest::{self, Url};

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct GetDeparturesResponse {
    pub stop: Stop,
    pub services: Vec<ServicesEntry>,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Stop {
    pub name: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServicesEntry {
    #[serde(rename = "ServiceID")]
    pub service_id: String,
    pub is_realtime: bool,
    pub destination_stop_name: String,
    pub display_departure_seconds: u64,
    pub departure_status: Option<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct NearbyStop {
    pub name: String,
    pub sms: String,
    pub lat: f64,
    pub long: f64,
}

impl Point for NearbyStop {
    fn coords(&self) -> (f64, f64) {
        (self.lat, self.long)
    }
}

#[derive(Debug, Fail)]
enum MetlinkError {
    #[fail(display = "bad http status code from metlink: {}", status)]
    BadStatus { status: reqwest::StatusCode },
}

fn ensure_ok(res: &reqwest::Response) -> Result<(), MetlinkError> {
    if !res.status().is_success() {
        Err(MetlinkError::BadStatus {
            status: res.status(),
        })?;
    }
    Ok(())
}

pub fn get_departures(stop_code: &str) -> Result<GetDeparturesResponse, failure::Error> {
    let mut url = Url::parse("https://www.metlink.org.nz/api/v1/StopDepartures").unwrap();
    url.path_segments_mut()
        .unwrap()
        .extend(std::iter::once(stop_code));

    let mut res = reqwest::get(url)?;
    ensure_ok(&res)?;

    Ok(res.json()?)
}

pub fn get_stops_nearby(lat: f64, lon: f64) -> Result<Vec<NearbyStop>, failure::Error> {
    let mut url = Url::parse("https://www.metlink.org.nz/api/v1/StopNearby").unwrap();

    {
        let mut path = url.path_segments_mut().unwrap();
        path.extend(std::iter::once(format!("{}", lat)));
        path.extend(std::iter::once(format!("{}", lon)));
    }

    let mut res = reqwest::get(url)?;
    ensure_ok(&res)?;

    Ok(res.json()?)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_parse_5000() {
        let data = include_str!("../testdata/5000.json");
        serde_json::from_str::<GetDeparturesResponse>(data).unwrap();
    }

    #[test]
    fn can_parse_well() {
        let data = include_str!("../testdata/WELL.json");
        serde_json::from_str::<GetDeparturesResponse>(data).unwrap();
    }
}
