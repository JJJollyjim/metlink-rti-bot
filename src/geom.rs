const EARTH_RADIUS_METRES: f64 = 6_781_000.0;

pub trait Point {
    fn coords(&self) -> (f64, f64);

    // fn sqr_distance_to_naive(self: &Self, other: &impl Point) -> f64 {
    //     let (self_lat, self_lon) = self.coords();
    //     let (other_lat, other_lon) = other.coords();

    //     ((self_lat - other_lat).powi(2) + (self_lon - other_lon).powi(2)).abs()
    // }

    fn distance_to(self: &Self, other: &impl Point) -> f64 {
        let (lat1, lon1) = self.coords();
        let (lat2, lon2) = other.coords();

        // TODO make nicer once #![feature(non_ascii_idents)] is stable

        let phi1 = lat1.to_radians();
        let phi2 = lat2.to_radians();

        let lam1 = lon1.to_radians();
        let lam2 = lon2.to_radians();

        let delta_phi = phi2 - phi1;
        let delta_lam = lam2 - lam1;

        // The square of half the chord length between the points
        let a = (delta_phi / 2.0).powi(2) + (phi1.cos() * phi2.cos() * (delta_lam / 2.0).powi(2));

        // The angular distance between the points
        let c = 2.0 * a.sqrt().atan2((1.0 - a).sqrt());

        // By d = r*θ
        c * EARTH_RADIUS_METRES
    }
}
