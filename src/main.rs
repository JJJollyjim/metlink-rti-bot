#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate failure;

mod geom;
mod metlink;
mod telegram;

use crossbeam_utils::thread;
use horrorshow::{append_html, html};
use lambda_http::http::{Response, StatusCode};
use lambda_http::{lambda, Body, Request};
use lambda_runtime::{error::HandlerError, Context};
use std::env;

use geom::Point;
use telegram::{ApiRequest, Chat, InlineKeyboardButton, ParseMode, Update, UpdateType};

#[derive(Deserialize, Serialize)]
enum InlineKeyboardPayload<'a> {
    NearbyStop(&'a str),
}

// #[derive(Debug, Fail)]
// enum MainError {
//     #[fail(display = "bad inline keyboard callback")]
//     BadCallback,
// }

fn main() {
    eprintln!("Main");
    lambda!(handler)
}

fn get_rti_message_for(sms: &str, chat: &Chat) -> Result<Body, serde_json::error::Error> {
    Ok(match metlink::get_departures(sms) {
        Ok(departures) => chat
            .make_message(&make_response_text(&departures), ParseMode::HTML)
            .to_response_body()?,
        Err(e) => chat
            .make_message(
                &format!("Error getting RTI for stop {}: {}", sms, e),
                ParseMode::Plain,
            )
            .to_response_body()?,
    })
}

fn handler(req: Request, _: Context) -> Result<Response<Body>, HandlerError> {
    eprintln!("Building client...");
    let tg_client = telegram::Client::new(&env::var("BOT_TOKEN").unwrap());

    eprintln!("Parsing update...");

    let update: Result<Update, serde_json::error::Error> = match req.body() {
        Body::Text(t) => serde_json::from_str(&t),
        Body::Binary(b) => serde_json::from_slice(&b),
        Body::Empty => Err("req body was empty")?,
    };

    match update {
        Ok(update) => {
            Ok(Response::builder()
           .status(StatusCode::OK)
           .body(
               if let UpdateType::Message(m) = update.update {
            if let Some(text) = m.text {
                if &text == "/start" {
                    m.chat.make_message("Send me a stop code (like 4129 or WELL) to receive live departure information, or share your location to get a list of nearby stops!\n\nSource code available at https://gitlab.com/JJJollyjim/metlink-rti-bot", ParseMode::Plain).to_response_body()?
                } else {
                    get_rti_message_for(&text.trim().to_uppercase(), &m.chat)?
                }
            } else if let Some(loc) = m.location {
                match metlink::get_stops_nearby(loc.latitude, loc.longitude) {
                    Ok(stops) => {
                        let mut stop_dists = stops.into_iter().map(|s| (s.distance_to(&loc), s)).collect::<Vec<_>>();

                        // TODO prove soundness of unwrap (i.e. that numbers will never be NaN, assuming well-behaved APIs)
                        stop_dists.sort_unstable_by(|(dist1, _), (dist2, _)| dist1.partial_cmp(dist2).unwrap());

                        let buttons = stop_dists.into_iter().take(5).map(|(dist, stop)| InlineKeyboardButton::new(
                            format!("{}: {} ({:.0}m)", stop.sms, stop.name, dist),
                            &InlineKeyboardPayload::NearbyStop(&stop.sms)
                        )).collect::<Vec<_>>();

                        m.chat.make_message_with_inline_keyboard(
                            "Click a nearby stop for departure information",
                            ParseMode::Plain,
                            buttons.into_iter().map(|b| vec!(b)).collect()
                        ).to_response_body()?
                    },
                    Err(e) => m.chat.make_message(
                        &format!("Error getting stops near your location: {}", e),
                        ParseMode::Plain
                    ).to_response_body()?,
                }
            } else {
                eprintln!("Non-text message");
                Body::Empty
            }
        } else if let UpdateType::CallbackQuery(cb) = update.update {
            let answer = cb.make_answer();
            thread::scope(|s| {
                s.spawn(|_| {
                    // We don't care too much if this succeeds or not
                    let _ = answer.execute(tg_client);
                });
            }).unwrap();

            let cb_data = cb.data.ok_or("bad callback")?;
            let payload = serde_json::from_str(&cb_data)?;

            match payload {
                InlineKeyboardPayload::NearbyStop(sms) =>
                    get_rti_message_for(sms, &cb.message.chat)?
            }
        } else {
            eprintln!("Unknown update type");
            Body::Empty
        })
        .map_err(|_| "error building response")?)
        }
        Err(e) => {
            eprintln!("Couldn't parse update: {}", e);
            Ok(Response::builder()
                .status(StatusCode::OK)
                .body(Body::Empty)
                .unwrap())
        }
    }
}

fn make_response_text(d: &metlink::GetDeparturesResponse) -> String {
    // TODO: use maud instead of horrowshow, for much nicer templating syntax

    let markup = html! {
        : "Departures from ";
        b : &d.stop.name;
        : ":\n";
        @ for s in d.services.iter() {
            : pick_emoji(s);
            : " ";
            b : &s.service_id;
            : " (";
            : &s.destination_stop_name;
            : "): ";
            : format!("{:.1} min", s.display_departure_seconds as f32 / 60_f32);
            @ if let Some(status) = &s.departure_status {
                @ if status != "onTime" {
                    i {
                        : " (";
                        : status;
                        : ")";
                    }
                }
            }
            : "\n";
        }
    };
    format!("{}", markup)
}

fn pick_emoji(s: &metlink::ServicesEntry) -> &'static str {
    // TODO clean this up slightly, figuring out how to satisfy the borrow checker...

    if let Some(status) = &s.departure_status {
        if status == "cancelled" {
            return "⛔";
        }
    }

    if s.is_realtime {
        "🔵"
    } else {
        "❔"
    }
}
