use super::request::AnswerCallbackQuery;
use super::types::*;

#[derive(Deserialize, Debug)]
pub struct Update {
    #[serde(rename = "update_id")]
    _update_id: u64,
    #[serde(flatten)]
    pub update: UpdateType,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
pub enum UpdateType {
    Message(Message),
    CallbackQuery(Callback),
}

#[derive(Deserialize, Debug)]
pub struct Message {
    #[serde(rename = "message_id")]
    _message_id: u64,
    pub text: Option<String>,
    pub location: Option<Location>,
    pub chat: Chat,
}

#[derive(Deserialize, Debug)]
pub struct Callback {
    id: String,
    pub data: Option<String>,
    pub message: Message,
}

impl Callback {
    pub fn make_answer(&self) -> AnswerCallbackQuery {
        AnswerCallbackQuery {
            callback_query_id: &self.id,
        }
    }
}
