use crate::geom::Point;
use crate::telegram::request::SendMessage;

#[derive(Deserialize, Debug)]
pub struct Location {
    pub latitude: f64,
    pub longitude: f64,
}

impl Point for Location {
    fn coords(&self) -> (f64, f64) {
        (self.latitude, self.longitude)
    }
}

#[derive(Deserialize, Debug)]
pub struct Chat {
    id: u64,
}

#[derive(Serialize, PartialEq, Eq)]
pub enum ParseMode {
    HTML,
    // Markdown,
    Plain,
}

impl ParseMode {
    pub fn is_plain(&self) -> bool {
        self == &ParseMode::Plain
    }
}

impl Default for ParseMode {
    fn default() -> Self {
        ParseMode::Plain
    }
}

impl Chat {
    // TODO builder interface to make messages
    pub fn make_message<'a>(&self, text: &'a str, parse_mode: ParseMode) -> SendMessage<'a> {
        SendMessage {
            chat_id: self.id,
            text,
            parse_mode,
            reply_to_message_id: None,
            reply_markup: ReplyMarkup::None,
        }
    }

    pub fn make_message_with_inline_keyboard<'a>(
        &self,
        text: &'a str,
        parse_mode: ParseMode,
        markup: Vec<Vec<InlineKeyboardButton>>,
    ) -> SendMessage<'a> {
        SendMessage {
            chat_id: self.id,
            text,
            parse_mode,
            reply_to_message_id: None,
            reply_markup: ReplyMarkup::InlineKeyboard(markup),
        }
    }
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum ReplyMarkup {
    None,
    InlineKeyboard(Vec<Vec<InlineKeyboardButton>>),
}

impl ReplyMarkup {
    pub fn is_none(&self) -> bool {
        if let ReplyMarkup::None = self {
            true
        } else {
            false
        }
    }
}

#[derive(Serialize)]
pub struct InlineKeyboardButton {
    pub text: String,
    pub callback_data: String,
}

impl InlineKeyboardButton {
    pub fn new(text: String, callback_data: &impl serde::ser::Serialize) -> Self {
        InlineKeyboardButton {
            text,
            callback_data: serde_json::to_string(callback_data).unwrap(),
        }
    }
}
