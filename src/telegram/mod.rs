use percent_encoding::{utf8_percent_encode, USERINFO_ENCODE_SET};
use reqwest::{self, Url};

mod request;
mod types;
mod webhook;

pub use request::ApiRequest;
pub use types::*;
pub use webhook::{Update, UpdateType};

#[derive(Debug)]
pub struct Client {
    http_client: reqwest::Client,
    base_url: Url,
}

impl Client {
    pub fn new(bot_token: &str) -> Self {
        let encoded_token = utf8_percent_encode(bot_token, USERINFO_ENCODE_SET);

        let bot_token = if bot_token.starts_with("bot") {
            format!("{}/", encoded_token)
        } else {
            format!("bot{}/", encoded_token)
        };

        Client {
            http_client: reqwest::Client::new(),
            base_url: Url::parse("https://api.telegram.org/")
                .unwrap()
                .join(&bot_token)
                .unwrap(),
        }
    }

    fn url_for<T>(&self) -> Url
    where
        T: ApiRequest,
    {
        self.base_url.join(T::PATH).unwrap()
    }
}
