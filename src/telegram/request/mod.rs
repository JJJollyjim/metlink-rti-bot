use lambda_http::Body;
use reqwest;
use serde::{de::DeserializeOwned, Serialize};
use serde_json;

use super::Client;

mod answer_callback_query;
mod send_message;

pub use answer_callback_query::AnswerCallbackQuery;
pub use send_message::SendMessage;

pub trait ApiRequest: std::marker::Sized + Serialize {
    const PATH: &'static str;
    type ResponseData: DeserializeOwned;

    fn execute(self, client: Client) -> Result<Self::ResponseData, failure::Error> {
        let mut res = client
            .http_client
            .post(client.url_for::<Self>())
            .json(&self)
            .send()?;

        if !res.status().is_success() {
            Err(BadStatus(res.status()))?
        }

        Ok(res.json()?)
    }

    fn to_response_body(self) -> Result<lambda_http::Body, serde_json::error::Error> {
        Ok(Body::Text(serde_json::to_string(&WebhookResponse {
            method: Self::PATH,
            api_request: self,
        })?))
    }
}

#[derive(Serialize)]
struct WebhookResponse<T: ApiRequest> {
    // To be provided by T::PATH
    method: &'static str,
    #[serde(flatten)]
    api_request: T,
}

#[derive(Debug, Fail)]
#[fail(display = "Bad response code: {}", _0)]
pub struct BadStatus(reqwest::StatusCode);
