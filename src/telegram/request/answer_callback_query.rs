use super::ApiRequest;

#[derive(Serialize)]
pub struct AnswerCallbackQuery<'a> {
    pub callback_query_id: &'a str,
}

impl<'a> ApiRequest for AnswerCallbackQuery<'a> {
    const PATH: &'static str = "answerCallbackQuery";
    type ResponseData = ();
}
