use super::ApiRequest;

use crate::telegram::types::*;

#[derive(Serialize)]
pub struct SendMessage<'a> {
    pub chat_id: u64,
    pub text: &'a str,
    #[serde(skip_serializing_if = "ParseMode::is_plain")]
    #[serde(default)]
    pub parse_mode: ParseMode,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reply_to_message_id: Option<u64>,
    #[serde(skip_serializing_if = "ReplyMarkup::is_none")]
    pub reply_markup: ReplyMarkup,
}

impl<'a> ApiRequest for SendMessage<'a> {
    const PATH: &'static str = "sendMessage";
    type ResponseData = ();
}
