# (Unofficial) Metlink RTI Telegram Bot

Since Telegram inexplicably works when you have no data on 2degrees, I made a bot to check bus/train arrivals!

It uses the [Serverless framework](https://serverless.com/framework/), which seems pretty cool so far!

## Usage

The main deployed version of the bot is available as [@metlinkrtibot](https://telegram.me/metlinkrtibot).

Currently it accepts stop codes (`4129`, `WELL` etc) and responds with RTI information.

## Deployment

First, configure an AWS admin token on your computer [like so](https://serverless.com/framework/docs/providers/aws/guide/credentials/).

Then generate a secret token and store it in AWS Secrets Manager, like so:

    aws --region ap-southeast-2 ssm put-parameter --name rtiTelegramBotPath --value $SOME_LONG_SECRET_STRING
    
This token forms part of the webhook url that Telegram sends messages to, so
ideally it is only known to you and Telegram.

The other configuration secret to store is the Telegram Bot API token:

    aws --region ap-southeast-2 ssm put-parameter --name rtiTelegramBotToken --value $SECRET_FROM_TELEGRAM

With the credentials stored, you can run:

    npm i
    npx serverless deploy
    
Which should build the application and deploy it to AWS via CloudFormation.
This command will print the url that will act as our webhook (in the "Endpoints" section)

To notify Telegram of the webhook url, perform an API request like so:

    curl --header "Content-Type: application/json" \
         --request POST \
         --data '{"url": "$THE_ENDPOINT_URL"}' \
         https://api.telegram.org/bot$YOUR_BOT_TOKEN/setWebhook 

## Performance

The bot is deployed to `ap-southeast-2`, since at time of writing this is where
Metlink's API server is running, so this produces the lowest latency.
Unfortunately, Metlink still takes ~900ms to respond to some API requests, which
is the major limiting factor on performance (and thus hosting cost).

## License

> Copyright (C) 2019  Jamie McClymont
> 
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
> 
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
